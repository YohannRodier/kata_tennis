public class Score {
    private int score1 = 0;
    private int score2 = 0;


    public String get_score() {
        if (score1 == 0  && score2 == 0) {
            return "0-0";
        }
        else if (score1 == 1  && score2 == 0){
            return "15-0";
        }
        else if (score1 == 2 && score2 == 0){
            return "30-0";
        }
        else if (score1 == 3 && score2 == 0){
            return "40-0";
        }

        else if (score1 == 0 && score2 == 1){
            return "0-15";
        }
        else if (score1 == 0 && score2 == 2){
            return "0-30";
        }
        else if (score1 == 0 && score2 == 3){
            return "0-40";
        }

        else if (score1 == 1 && score2 == 1 ){
            return "15-15";
        }
        else if (score1 == 2 && score2 == 1){
            return "30-15";
        }
        else if (score1 == 3 && score2 == 1){
            return "40-15";
        }

        else if (score1 == 1 && score2 == 2 ) {
            return "15-30";
        }

        else if (score1 == 2 && score2 == 2){
            return "30-30";
        }
        else if (score1 == 3 && score2 == 2){
            return "40-30";
        }

        else if (score1 == 1 && score2 == 3 ){
            return "15-40";
        }
        else if (score1 == 2 && score2 == 3){
            return "30-40";
        }
        else if (score1 == 3 && score2 == 3){
            return "40-40";
        }
        else if (score1 == 4 && score2 == 3) {
            return "A-40";
        }
        else{
            return "40-A";
        }
    }

    public void playerScorePoint(int player){
        if (player == 1 && score2 < 4){
            score1++;
        }
        else if (player == 1 && score2 == 4){
            score2--;
        }
        else if (player == 2 && score1 < 4){
            score2++;
        }
        else{
            score1--;
        }
    }
}
