import junit.framework.TestCase;

public class Test extends TestCase {
    public void testScore0_0() {
        Score score = new Score();
        assertEquals("0-0", score.get_score());
    }

    public void testScore15_0(){
        Score score = new Score();
        score.playerScorePoint(1);
        assertEquals("15-0", score.get_score());
    }

    public void testScore30_0(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        assertEquals("30-0", score.get_score());
    }

    public void testScore40_0(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        assertEquals("40-0", score.get_score() );
    }

    public void testScore0_15(){
        Score score = new Score();
        score.playerScorePoint(2);
        assertEquals("0-15", score.get_score() );
    }

    public void testScore0_30(){
        Score score = new Score();
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("0-30", score.get_score() );
    }

    public void testScore0_40(){
        Score score = new Score();
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("0-40", score.get_score());
    }

    public void testScore15_15(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        assertEquals("15-15", score.get_score());
    }

    public void testScore30_15(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        assertEquals("30-15", score.get_score());
    }

    public void testScore40_15(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        assertEquals("40-15", score.get_score());
    }

    public void testScore15_30(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("15-30", score.get_score());
    }

    public void testScore15_40(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("15-40", score.get_score());
    }

    public void testScore30_30(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("30-30", score.get_score());
    }

    public void testScore30_40(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("30-40", score.get_score());
    }

    public void testScore40_30(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("40-30", score.get_score());
    }

    public void testScore40_40(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("40-40", score.get_score());
    }

    public void testA_40(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(1);
        assertEquals("A-40", score.get_score());
    }

    public void test40_A(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        assertEquals("40-A", score.get_score());
    }

    public void testPlayer1LooseAdvantage(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        assertEquals("40-40", score.get_score());
    }

    public void testPlayer2LooseAdvantage(){
        Score score = new Score();
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(1);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(2);
        score.playerScorePoint(1);
        assertEquals("40-40", score.get_score());
    }
}
